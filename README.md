# cro

[cro](https://modules.raku.org/dist/cro:cpan:JNTHN) Libraries for building reactive distributed systems. [cro.services](https://cro.services)

* [*Day 6: Declarative APIs, easy peasy with Raku*
  ](https://raku-advent.blog/2020/12/06/day-6-declarative-apis-easy-peasy-with-raku/)
  2020-12 Moritz
* [*Day 2 – CRUD with Cro::HTTP, a tutorial*
  ](https://raku-advent.blog/2019/12/02/crud-cro-http-tutorial/)
  2019-12 koto
* [*Day 22 – Testing Cro HTTP APIs*
  ](https://perl6advent.wordpress.com/2018/12/22/day-22-testing-cro-http-apis/)
  2018-12 jnthnwrthngtn
* [*Day 9 – HTTP and Web Sockets with Cro*
  ](https://perl6advent.wordpress.com/2017/12/09/http-and-web-sockets-with-cro/)
  2017-12 jnthnwrthngtn
